## GenderGenerator
### Windows Users
Download git repo and run GenderGenerator.exe

### Build / Compilation
Download Golang compiler, and run go build main.go

### Instructions
Press ENTER to Generate more sequences, or Ctrl+c to exit the loop.