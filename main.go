/*
Copyright 2016 Miegenic
 */

package main

import (
	"strings"
	"io/ioutil"
	"log"
	"fmt"
	"math/rand"
	"time"
	"strconv"
	"bufio"
	"os"
)

func txtToSlice(input string) []string {
	return strings.Split(input, "\n")
}

func getWord(wordlist []string) string {
	return wordlist[rand.Intn(len(wordlist))]
}

func init() {
	rand.Seed(time.Now().UnixNano())
}

func generateGender() string {
	return randomGeneration() + getWord(returnWords("adjectives")) + " " + getWord(returnWords("nouns")) + "\n"
}

func main() {
	if len(os.Args) == 1 {
		for {
			for i := 0; i <= 15; i++ {
				go func() {
					fmt.Print(generateGender())
				}()
			}
			bufio.NewReader(os.Stdin).ReadBytes('\n')
		}
	} else {
		if (os.Args[1] == "all") {
			for i := 0; i <= 10; i++ {
				for _, adj := range returnWords("adjectives") {
					for _, noun := range returnWords("nouns") {
						fmt.Print(strconv.Itoa(i) + "th generation " + string(adj) + " " + string(noun) + "\n")
					}
				}
			}
		} else {
			for {
				for i := 0; i <= 15; i++ {
					fmt.Print(generateGender())
				}
				bufio.NewReader(os.Stdin).ReadBytes('\n')
			}
		}
	}
}


func returnWords(name string) []string {

	dat, err := ioutil.ReadFile(name)

	if err != nil {
		log.Fatal(err)
	}

	return txtToSlice(string(dat))
}

func randomGeneration() string {
	rand := rand.Intn(9) + 1
	after := ""
	if rand == 1 {
		after = "st"
	} else if rand == 2 {
		after = "nd"
	} else if rand == 3 {
		after = "rd"
	} else {
		after = "th"
	}
	return strconv.Itoa(rand) + after + " generation "
}